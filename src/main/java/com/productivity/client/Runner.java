package com.productivity.client;

import com.productivity.config.ConfigurationHelper;
import com.productivity.config.QueriesConfig;
import com.productivity.config.SMTPMailThreadConfig;
import com.productivity.dao.DaoRepository;
import com.productivity.dto.VolumeMetricsReport;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.File;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Component
public class Runner implements ApplicationRunner {

    public static final Logger logger = LoggerFactory.getLogger(Runner.class);

    private QueriesConfig queriesConfig;

    private SMTPMailThreadConfig smtpMailThreadConfig;

    private Session session;

    private DaoRepository daoRepository;

    private TemplateEngine templateEngine;

    private ConfigurationHelper configurationHelper;

    @Autowired
    public Runner(QueriesConfig queriesConfig,
                  SMTPMailThreadConfig smtpMailThreadConfig,
                  Session session,
                  DaoRepository daoRepository,
                  TemplateEngine templateEngine,
                  ConfigurationHelper configurationHelper) {

        this.queriesConfig = queriesConfig;
        this.smtpMailThreadConfig = smtpMailThreadConfig;
        this.session = session;
        this.daoRepository = daoRepository;
        this.templateEngine = templateEngine;
        this.configurationHelper = configurationHelper;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {

        System.out.println("===Inside Run Method===");
        logger.info("===Inside Run method===");

        List<String> businessDates = args.getOptionValues("businessDate");
        Date businessDate;
        String bdString;
        if (businessDates != null && businessDates.size() > 0) {

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            bdString = businessDates.get(0);
            businessDate = dateFormat.parse(bdString);

        } else {

            Calendar instance = Calendar.getInstance();
            instance.add(Calendar.DAY_OF_MONTH, -1);
            businessDate = instance.getTime();
            System.out.println("businessDate = " + businessDate);
        }

        Calendar businessDateCal = Calendar.getInstance();
        businessDateCal.setTime(businessDate);
        businessDateCal.set(businessDateCal.get(Calendar.YEAR), businessDateCal.get(Calendar.MONTH), businessDateCal.get(Calendar.DAY_OF_MONTH), 0, 0, 0);

        Calendar nextBusinessDate = Calendar.getInstance();
        nextBusinessDate.setTime(businessDate);
        nextBusinessDate.add(Calendar.DAY_OF_MONTH, 1);
        nextBusinessDate.set(nextBusinessDate.get(Calendar.YEAR), nextBusinessDate.get(Calendar.MONTH), nextBusinessDate.get(Calendar.DAY_OF_MONTH), 0, 0, 0);


        Calendar previousMonthStart = Calendar.getInstance();
        previousMonthStart.setTime(businessDate);
        previousMonthStart.add(Calendar.MONTH, -1);
        previousMonthStart.set(previousMonthStart.get(Calendar.YEAR), previousMonthStart.get(Calendar.MONTH), 1, 0, 0, 0);

        Calendar thisMonthStart = Calendar.getInstance();
        thisMonthStart.setTime(businessDate);
        thisMonthStart.set(thisMonthStart.get(Calendar.YEAR), thisMonthStart.get(Calendar.MONTH), 1, 0, 0, 0);

        Calendar previousMonthEnd = Calendar.getInstance();
        previousMonthEnd.setTime(thisMonthStart.getTime());
        previousMonthEnd.add(Calendar.DAY_OF_MONTH,-1);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        VolumeMetricsReport volumeMetricsReport = getVolumeMetricsReport(businessDate,nextBusinessDate.getTime(),thisMonthStart.getTime(),previousMonthStart.getTime(),previousMonthEnd.getTime());

        StringWriter writer = new StringWriter();
        Context context = new Context();
        context.setVariable("vmr", volumeMetricsReport);
        templateEngine.process("report", context, writer);

        //SENDING A MAIL TO RECIPIENTS
        List<String> listOfFiles = new ArrayList<>();
        listOfFiles.add(queriesConfig.getExcelDumpFileLocationOne() +"MakerDump.xlsx");
        listOfFiles.add(queriesConfig.getExcelDumpFileLocationTwo() +"CheckerDump.xlsx");
        sendMailWithAttachment(smtpMailThreadConfig.getSubject(), writer.toString(), smtpMailThreadConfig.getToAddress(),
                smtpMailThreadConfig.getCcAddress(), listOfFiles);

    }//Run()

    public VolumeMetricsReport getVolumeMetricsReport(Date businessDate,Date nextBusinessDate, Date thisMonthStart, Date previousMonthStart, Date previousMonthEnd) throws Exception {

        System.out.println("Inside get VMR method");
        VolumeMetricsReport volumeMetricsReport = new VolumeMetricsReport();

        daoRepository.getDataForMakerAndCheckerTable(volumeMetricsReport,businessDate,nextBusinessDate,thisMonthStart,previousMonthStart,previousMonthEnd);
        daoRepository. executeAllQueriesForDump(businessDate,nextBusinessDate,thisMonthStart,previousMonthStart,previousMonthEnd);
        //Todo: check correct or not below and dumpExcelQueries above
        volumeMetricsReport.calculateTotalRow();

        return volumeMetricsReport;
    }


    /**
     * This method will send mail with multiple attachments
     *
     * @param subject
     * @param body
     * @param toEmail
     * @param ccEmail
     * @param fileName {Contains attachment paths EX: /home/user/attacment.zip}
     */

    public void sendMailWithAttachment(String subject, String body, String toEmail, String ccEmail, List<String> fileName) {

        logger.info("subject:{},body:{},toEmail:{},ccEmail:{},fileName:{}", subject, body, toEmail, ccEmail, fileName);

        try {
            logger.info("Sending a mail.");

            System.out.println("Inside sendMailWithAttach tryblock");
            MimeMessage msg = new MimeMessage(session);
            //set message headers
            msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
            //msg.addHeader("Content-type", "application/excel; charset=UTF-8");
            msg.addHeader("format", "flowed");
            msg.addHeader("Content-Transfer-Encoding", "8bit");

            msg.setFrom(new InternetAddress(smtpMailThreadConfig.getFromAddress(), smtpMailThreadConfig.getFromName()));

            msg.setReplyTo(InternetAddress.parse(smtpMailThreadConfig.getFromAddress(), false));

            msg.setSubject(subject, "UTF-8");

            // msg.setContent(body, "text/html");


            MimeBodyPart messageBodyPart1 = new MimeBodyPart();

            messageBodyPart1.addHeader("Content-type", "text/HTML; charset=UTF-8");
            messageBodyPart1.addHeader("format", "flowed");
            messageBodyPart1.addHeader("Content-Transfer-Encoding", "8bit");
            //messageBodyPart1.setText(st.toString());
            messageBodyPart1.setContent(body, "text/html");

            Multipart multipart = new MimeMultipart();

            for (String attachmentFileName : fileName) {
                addAttachment(multipart, attachmentFileName);

            }

            multipart.addBodyPart(messageBodyPart1);
            msg.setContent(multipart);
            msg.setSentDate(new Date());
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail, false));
            msg.setRecipients(Message.RecipientType.CC, InternetAddress.parse(ccEmail, false));
            Transport.send(msg);

        } catch (Exception e) {

            logger.error("ERROR RAISED WHILE SENDING MAIL WITH AN ATTACHMENT..=" + ExceptionUtils.getStackTrace(e));
            throw new RuntimeException(e);
        }
    }

    /**
     * Method will add attachments to Multipart
     *
     * @param multipart
     * @param fileName
     * @throws MessagingException
     */
    public void addAttachment(Multipart multipart, String fileName) throws MessagingException {

        System.out.println("Inside add Attacment");
        DataSource source = new FileDataSource(fileName);
        BodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setDataHandler(new DataHandler(source));
        messageBodyPart.setFileName(new File(fileName).getName());
        multipart.addBodyPart(messageBodyPart);

    }


}