package com.productivity.dto;

public class Activities {

    //Todo: //////////////////////////No.of.Edms for each Each activity per maker/Checker/////////////////////////////////////////////////////////////////////////////////////////////
    
    private int makerEdmsAccountOpeningFlex;
    private int makerEdmsAccountOpeningDob;
    private int makerEdmsKycFlex;
    private int makerEdmsKycBpm;
    private int makerEdmsKycUnfreezeFlex;
    private int makerEdmsAccountClosure;
    private int makerEdmsFixedDepositsForBreakageRenewals;
    private int makerEdmsFixedDepositsForGtsStp;
    private int makerEdmsSignatureMandateAdditionOrAmmendment;
    private int makerEdmsTradeLicense;
    private int makerEdmsAccountMaintanenceMisc;
    private int makerEdmsChequeBookGtsStp;
    
    private int makerEdmsTotalTransactionsRow;

    private int makerEdmsMtd;
    private int makerEdmsPreviousMonth;

    private int checkerEdmsAccountOpeningFlex;
    private int checkerEdmsAccountOpeningDob;
    private int checkerEdmsKycFlex;
    private int checkerEdmsKycBpm;
    private int checkerEdmsKycUnfreezeFlex;
    private int checkerEdmsAccountClosure;
    private int checkerEdmsFixedDepositsForBreakageRenewals;
    private int checkerEdmsFixedDepositsForGtsStp;
    private int checkerEdmsSignatureMandateAdditionOrAmmendment;
    private int checkerEdmsTradeLicense;
    private int checkerEdmsAccountMaintanenceMisc;
    private int checkerEdmsChequeBookGtsStp;

    private int checkerEdmsTotalTransactionsRow;

    private int checkerEdmsMtd;
    private int checkerEdmsPreviousMonth;
    
 //Todo: //////////////////////////No.ofTransactions for each Each activity per maker/Checker/////////////////////////////////////////////////////////////////////////////////////////////

    private int makerTransactionsAccountOpeningFlex;
    private int makerTransactionsAccountOpeningDob;
    private int makerTransactionsKycFlex;
    private int makerTransactionsKycBpm;
    private int makerTransactionsKycUnfreezeFlex;
    private int makerTransactionsAccountClosure;
    private int makerTransactionsFixedDepositsForBreakageRenewals;
    private int makerTransactionsFixedDepositsForGtsStp;
    private int makerTransactionsSignatureMandateAdditionOrAmmendment;
    private int makerTransactionsTradeLicense;
    private int makerTransactionsAccountMaintanenceMisc;
    private int makerTransactionsChequeBookGtsStp;
    
    private int makerTransactionsTotalTransactionsRow;
    
    private int makerTransactionsMtd;
    private int makerTransactionsPreviousMonth;
    
    private int checkerTransactionsAccountOpeningFlex;
    private int checkerTransactionsAccountOpeningDob;
    private int checkerTransactionsKycFlex;
    private int checkerTransactionsKycBpm;
    private int checkerTransactionsKycUnfreezeFlex;
    private int checkerTransactionsAccountClosure;
    private int checkerTransactionsFixedDepositsForBreakageRenewals;
    private int checkerTransactionsFixedDepositsForGtsStp;
    private int checkerTransactionsSignatureMandateAdditionOrAmmendment;
    private int checkerTransactionsTradeLicense;
    private int checkerTransactionsAccountMaintanenceMisc;
    private int checkerTransactionsChequeBookGtsStp;
    
    private int checkerTransactionsTotalTransactionsRow;
    
    private int checkerTransactionsMtd;
    private int checkerTransactionsPreviousMonth;

    public int getMakerEdmsAccountOpeningFlex() {
        return makerEdmsAccountOpeningFlex;
    }

    public void setMakerEdmsAccountOpeningFlex(int makerEdmsAccountOpeningFlex) {
        this.makerEdmsAccountOpeningFlex = makerEdmsAccountOpeningFlex;
    }

    public int getMakerEdmsAccountOpeningDob() {
        return makerEdmsAccountOpeningDob;
    }

    public void setMakerEdmsAccountOpeningDob(int makerEdmsAccountOpeningDob) {
        this.makerEdmsAccountOpeningDob = makerEdmsAccountOpeningDob;
    }

    public int getMakerEdmsKycFlex() {
        return makerEdmsKycFlex;
    }

    public void setMakerEdmsKycFlex(int makerEdmsKycFlex) {
        this.makerEdmsKycFlex = makerEdmsKycFlex;
    }

    public int getMakerEdmsKycBpm() {
        return makerEdmsKycBpm;
    }

    public void setMakerEdmsKycBpm(int makerEdmsKycBpm) {
        this.makerEdmsKycBpm = makerEdmsKycBpm;
    }

    public int getMakerEdmsKycUnfreezeFlex() {
        return makerEdmsKycUnfreezeFlex;
    }

    public void setMakerEdmsKycUnfreezeFlex(int makerEdmsKycUnfreezeFlex) {
        this.makerEdmsKycUnfreezeFlex = makerEdmsKycUnfreezeFlex;
    }

    public int getMakerEdmsAccountClosure() {
        return makerEdmsAccountClosure;
    }

    public void setMakerEdmsAccountClosure(int makerEdmsAccountClosure) {
        this.makerEdmsAccountClosure = makerEdmsAccountClosure;
    }

    public int getMakerEdmsFixedDepositsForBreakageRenewals() {
        return makerEdmsFixedDepositsForBreakageRenewals;
    }

    public void setMakerEdmsFixedDepositsForBreakageRenewals(int makerEdmsFixedDepositsForBreakageRenewals) {
        this.makerEdmsFixedDepositsForBreakageRenewals = makerEdmsFixedDepositsForBreakageRenewals;
    }

    public int getMakerEdmsFixedDepositsForGtsStp() {
        return makerEdmsFixedDepositsForGtsStp;
    }

    public void setMakerEdmsFixedDepositsForGtsStp(int makerEdmsFixedDepositsForGtsStp) {
        this.makerEdmsFixedDepositsForGtsStp = makerEdmsFixedDepositsForGtsStp;
    }

    public int getMakerEdmsSignatureMandateAdditionOrAmmendment() {
        return makerEdmsSignatureMandateAdditionOrAmmendment;
    }

    public void setMakerEdmsSignatureMandateAdditionOrAmmendment(int makerEdmsSignatureMandateAdditionOrAmmendment) {
        this.makerEdmsSignatureMandateAdditionOrAmmendment = makerEdmsSignatureMandateAdditionOrAmmendment;
    }

    public int getMakerEdmsTradeLicense() {
        return makerEdmsTradeLicense;
    }

    public void setMakerEdmsTradeLicense(int makerEdmsTradeLicense) {
        this.makerEdmsTradeLicense = makerEdmsTradeLicense;
    }

    public int getMakerEdmsAccountMaintanenceMisc() {
        return makerEdmsAccountMaintanenceMisc;
    }

    public void setMakerEdmsAccountMaintanenceMisc(int makerEdmsAccountMaintanenceMisc) {
        this.makerEdmsAccountMaintanenceMisc = makerEdmsAccountMaintanenceMisc;
    }

    public int getMakerEdmsChequeBookGtsStp() {
        return makerEdmsChequeBookGtsStp;
    }

    public void setMakerEdmsChequeBookGtsStp(int makerEdmsChequeBookGtsStp) {
        this.makerEdmsChequeBookGtsStp = makerEdmsChequeBookGtsStp;
    }

    public int getMakerEdmsTotalTransactionsRow() {
        return makerEdmsTotalTransactionsRow;
    }

    public void setMakerEdmsTotalTransactionsRow(int makerEdmsTotalTransactionsRow) {
        this.makerEdmsTotalTransactionsRow = makerEdmsTotalTransactionsRow;
    }

    public int getMakerEdmsMtd() {
        return makerEdmsMtd;
    }

    public void setMakerEdmsMtd(int makerEdmsMtd) {
        this.makerEdmsMtd = makerEdmsMtd;
    }

    public int getMakerEdmsPreviousMonth() {
        return makerEdmsPreviousMonth;
    }

    public void setMakerEdmsPreviousMonth(int makerEdmsPreviousMonth) {
        this.makerEdmsPreviousMonth = makerEdmsPreviousMonth;
    }

    public int getCheckerEdmsAccountOpeningFlex() {
        return checkerEdmsAccountOpeningFlex;
    }

    public void setCheckerEdmsAccountOpeningFlex(int checkerEdmsAccountOpeningFlex) {
        this.checkerEdmsAccountOpeningFlex = checkerEdmsAccountOpeningFlex;
    }

    public int getCheckerEdmsAccountOpeningDob() {
        return checkerEdmsAccountOpeningDob;
    }

    public void setCheckerEdmsAccountOpeningDob(int checkerEdmsAccountOpeningDob) {
        this.checkerEdmsAccountOpeningDob = checkerEdmsAccountOpeningDob;
    }

    public int getCheckerEdmsKycFlex() {
        return checkerEdmsKycFlex;
    }

    public void setCheckerEdmsKycFlex(int checkerEdmsKycFlex) {
        this.checkerEdmsKycFlex = checkerEdmsKycFlex;
    }

    public int getCheckerEdmsKycBpm() {
        return checkerEdmsKycBpm;
    }

    public void setCheckerEdmsKycBpm(int checkerEdmsKycBpm) {
        this.checkerEdmsKycBpm = checkerEdmsKycBpm;
    }

    public int getCheckerEdmsKycUnfreezeFlex() {
        return checkerEdmsKycUnfreezeFlex;
    }

    public void setCheckerEdmsKycUnfreezeFlex(int checkerEdmsKycUnfreezeFlex) {
        this.checkerEdmsKycUnfreezeFlex = checkerEdmsKycUnfreezeFlex;
    }

    public int getCheckerEdmsAccountClosure() {
        return checkerEdmsAccountClosure;
    }

    public void setCheckerEdmsAccountClosure(int checkerEdmsAccountClosure) {
        this.checkerEdmsAccountClosure = checkerEdmsAccountClosure;
    }

    public int getCheckerEdmsFixedDepositsForBreakageRenewals() {
        return checkerEdmsFixedDepositsForBreakageRenewals;
    }

    public void setCheckerEdmsFixedDepositsForBreakageRenewals(int checkerEdmsFixedDepositsForBreakageRenewals) {
        this.checkerEdmsFixedDepositsForBreakageRenewals = checkerEdmsFixedDepositsForBreakageRenewals;
    }

    public int getCheckerEdmsFixedDepositsForGtsStp() {
        return checkerEdmsFixedDepositsForGtsStp;
    }

    public void setCheckerEdmsFixedDepositsForGtsStp(int checkerEdmsFixedDepositsForGtsStp) {
        this.checkerEdmsFixedDepositsForGtsStp = checkerEdmsFixedDepositsForGtsStp;
    }

    public int getCheckerEdmsSignatureMandateAdditionOrAmmendment() {
        return checkerEdmsSignatureMandateAdditionOrAmmendment;
    }

    public void setCheckerEdmsSignatureMandateAdditionOrAmmendment(int checkerEdmsSignatureMandateAdditionOrAmmendment) {
        this.checkerEdmsSignatureMandateAdditionOrAmmendment = checkerEdmsSignatureMandateAdditionOrAmmendment;
    }

    public int getCheckerEdmsTradeLicense() {
        return checkerEdmsTradeLicense;
    }

    public void setCheckerEdmsTradeLicense(int checkerEdmsTradeLicense) {
        this.checkerEdmsTradeLicense = checkerEdmsTradeLicense;
    }

    public int getCheckerEdmsAccountMaintanenceMisc() {
        return checkerEdmsAccountMaintanenceMisc;
    }

    public void setCheckerEdmsAccountMaintanenceMisc(int checkerEdmsAccountMaintanenceMisc) {
        this.checkerEdmsAccountMaintanenceMisc = checkerEdmsAccountMaintanenceMisc;
    }

    public int getCheckerEdmsChequeBookGtsStp() {
        return checkerEdmsChequeBookGtsStp;
    }

    public void setCheckerEdmsChequeBookGtsStp(int checkerEdmsChequeBookGtsStp) {
        this.checkerEdmsChequeBookGtsStp = checkerEdmsChequeBookGtsStp;
    }

    public int getCheckerEdmsTotalTransactionsRow() {
        return checkerEdmsTotalTransactionsRow;
    }

    public void setCheckerEdmsTotalTransactionsRow(int checkerEdmsTotalTransactions) {
        this.checkerEdmsTotalTransactionsRow = checkerEdmsTotalTransactions;
    }

    public int getCheckerEdmsMtd() {
        return checkerEdmsMtd;
    }

    public void setCheckerEdmsMtd(int checkerEdmsMtd) {
        this.checkerEdmsMtd = checkerEdmsMtd;
    }

    public int getCheckerEdmsPreviousMonth() {
        return checkerEdmsPreviousMonth;
    }

    public void setCheckerEdmsPreviousMonth(int checkerEdmsPreviousMonth) {
        this.checkerEdmsPreviousMonth = checkerEdmsPreviousMonth;
    }

    public int getMakerTransactionsAccountOpeningFlex() {
        return makerTransactionsAccountOpeningFlex;
    }

    public void setMakerTransactionsAccountOpeningFlex(int makerTransactionsAccountOpeningFlex) {
        this.makerTransactionsAccountOpeningFlex = makerTransactionsAccountOpeningFlex;
    }

    public int getMakerTransactionsAccountOpeningDob() {
        return makerTransactionsAccountOpeningDob;
    }

    public void setMakerTransactionsAccountOpeningDob(int makerTransactionsAccountOpeningDob) {
        this.makerTransactionsAccountOpeningDob = makerTransactionsAccountOpeningDob;
    }

    public int getMakerTransactionsKycFlex() {
        return makerTransactionsKycFlex;
    }

    public void setMakerTransactionsKycFlex(int makerTransactionsKycFlex) {
        this.makerTransactionsKycFlex = makerTransactionsKycFlex;
    }

    public int getMakerTransactionsKycBpm() {
        return makerTransactionsKycBpm;
    }

    public void setMakerTransactionsKycBpm(int makerTransactionsKycBpm) {
        this.makerTransactionsKycBpm = makerTransactionsKycBpm;
    }

    public int getMakerTransactionsKycUnfreezeFlex() {
        return makerTransactionsKycUnfreezeFlex;
    }

    public void setMakerTransactionsKycUnfreezeFlex(int makerTransactionsKycUnfreezeFlex) {
        this.makerTransactionsKycUnfreezeFlex = makerTransactionsKycUnfreezeFlex;
    }

    public int getMakerTransactionsAccountClosure() {
        return makerTransactionsAccountClosure;
    }

    public void setMakerTransactionsAccountClosure(int makerTransactionsAccountClosure) {
        this.makerTransactionsAccountClosure = makerTransactionsAccountClosure;
    }

    public int getMakerTransactionsFixedDepositsForBreakageRenewals() {
        return makerTransactionsFixedDepositsForBreakageRenewals;
    }

    public void setMakerTransactionsFixedDepositsForBreakageRenewals(int makerTransactionsFixedDepositsForBreakageRenewals) {
        this.makerTransactionsFixedDepositsForBreakageRenewals = makerTransactionsFixedDepositsForBreakageRenewals;
    }

    public int getMakerTransactionsFixedDepositsForGtsStp() {
        return makerTransactionsFixedDepositsForGtsStp;
    }

    public void setMakerTransactionsFixedDepositsForGtsStp(int makerTransactionsFixedDepositsForGtsStp) {
        this.makerTransactionsFixedDepositsForGtsStp = makerTransactionsFixedDepositsForGtsStp;
    }

    public int getMakerTransactionsSignatureMandateAdditionOrAmmendment() {
        return makerTransactionsSignatureMandateAdditionOrAmmendment;
    }

    public void setMakerTransactionsSignatureMandateAdditionOrAmmendment(int makerTransactionsSignatureMandateAdditionOrAmmendment) {
        this.makerTransactionsSignatureMandateAdditionOrAmmendment = makerTransactionsSignatureMandateAdditionOrAmmendment;
    }

    public int getMakerTransactionsTradeLicense() {
        return makerTransactionsTradeLicense;
    }

    public void setMakerTransactionsTradeLicense(int makerTransactionsTradeLicense) {
        this.makerTransactionsTradeLicense = makerTransactionsTradeLicense;
    }

    public int getMakerTransactionsAccountMaintanenceMisc() {
        return makerTransactionsAccountMaintanenceMisc;
    }

    public void setMakerTransactionsAccountMaintanenceMisc(int makerTransactionsAccountMaintanenceMisc) {
        this.makerTransactionsAccountMaintanenceMisc = makerTransactionsAccountMaintanenceMisc;
    }

    public int getMakerTransactionsChequeBookGtsStp() {
        return makerTransactionsChequeBookGtsStp;
    }

    public void setMakerTransactionsChequeBookGtsStp(int makerTransactionsChequeBookGtsStp) {
        this.makerTransactionsChequeBookGtsStp = makerTransactionsChequeBookGtsStp;
    }

    public int getMakerTransactionsTotalTransactionsRow() {
        return makerTransactionsTotalTransactionsRow;
    }

    public void setMakerTransactionsTotalTransactionsRow(int makerTransactionsTotalTransactionsRow) {
        this.makerTransactionsTotalTransactionsRow = makerTransactionsTotalTransactionsRow;
    }

    public int getMakerTransactionsMtd() {
        return makerTransactionsMtd;
    }

    public void setMakerTransactionsMtd(int makerTransactionsMtd) {
        this.makerTransactionsMtd = makerTransactionsMtd;
    }

    public int getMakerTransactionsPreviousMonth() {
        return makerTransactionsPreviousMonth;
    }

    public void setMakerTransactionsPreviousMonth(int makerTransactionsPreviousMonth) {
        this.makerTransactionsPreviousMonth = makerTransactionsPreviousMonth;
    }

    public int getCheckerTransactionsAccountOpeningFlex() {
        return checkerTransactionsAccountOpeningFlex;
    }

    public void setCheckerTransactionsAccountOpeningFlex(int checkerTransactionsAccountOpeningFlex) {
        this.checkerTransactionsAccountOpeningFlex = checkerTransactionsAccountOpeningFlex;
    }

    public int getCheckerTransactionsAccountOpeningDob() {
        return checkerTransactionsAccountOpeningDob;
    }

    public void setCheckerTransactionsAccountOpeningDob(int checkerTransactionsAccountOpeningDob) {
        this.checkerTransactionsAccountOpeningDob = checkerTransactionsAccountOpeningDob;
    }

    public int getCheckerTransactionsKycFlex() {
        return checkerTransactionsKycFlex;
    }

    public void setCheckerTransactionsKycFlex(int checkerTransactionsKycFlex) {
        this.checkerTransactionsKycFlex = checkerTransactionsKycFlex;
    }

    public int getCheckerTransactionsKycBpm() {
        return checkerTransactionsKycBpm;
    }

    public void setCheckerTransactionsKycBpm(int checkerTransactionsKycBpm) {
        this.checkerTransactionsKycBpm = checkerTransactionsKycBpm;
    }

    public int getCheckerTransactionsKycUnfreezeFlex() {
        return checkerTransactionsKycUnfreezeFlex;
    }

    public void setCheckerTransactionsKycUnfreezeFlex(int checkerTransactionsKycUnfreezeFlex) {
        this.checkerTransactionsKycUnfreezeFlex = checkerTransactionsKycUnfreezeFlex;
    }

    public int getCheckerTransactionsAccountClosure() {
        return checkerTransactionsAccountClosure;
    }

    public void setCheckerTransactionsAccountClosure(int checkerTransactionsAccountClosure) {
        this.checkerTransactionsAccountClosure = checkerTransactionsAccountClosure;
    }

    public int getCheckerTransactionsFixedDepositsForBreakageRenewals() {
        return checkerTransactionsFixedDepositsForBreakageRenewals;
    }

    public void setCheckerTransactionsFixedDepositsForBreakageRenewals(int checkerTransactionsFixedDepositsForBreakageRenewals) {
        this.checkerTransactionsFixedDepositsForBreakageRenewals = checkerTransactionsFixedDepositsForBreakageRenewals;
    }

    public int getCheckerTransactionsFixedDepositsForGtsStp() {
        return checkerTransactionsFixedDepositsForGtsStp;
    }

    public void setCheckerTransactionsFixedDepositsForGtsStp(int checkerTransactionsFixedDepositsForGtsStp) {
        this.checkerTransactionsFixedDepositsForGtsStp = checkerTransactionsFixedDepositsForGtsStp;
    }

    public int getCheckerTransactionsSignatureMandateAdditionOrAmmendment() {
        return checkerTransactionsSignatureMandateAdditionOrAmmendment;
    }

    public void setCheckerTransactionsSignatureMandateAdditionOrAmmendment(int checkerTransactionsSignatureMandateAdditionOrAmmendment) {
        this.checkerTransactionsSignatureMandateAdditionOrAmmendment = checkerTransactionsSignatureMandateAdditionOrAmmendment;
    }

    public int getCheckerTransactionsTradeLicense() {
        return checkerTransactionsTradeLicense;
    }

    public void setCheckerTransactionsTradeLicense(int checkerTransactionsTradeLicense) {
        this.checkerTransactionsTradeLicense = checkerTransactionsTradeLicense;
    }

    public int getCheckerTransactionsAccountMaintanenceMisc() {
        return checkerTransactionsAccountMaintanenceMisc;
    }

    public void setCheckerTransactionsAccountMaintanenceMisc(int checkerTransactionsAccountMaintanenceMisc) {
        this.checkerTransactionsAccountMaintanenceMisc = checkerTransactionsAccountMaintanenceMisc;
    }

    public int getCheckerTransactionsChequeBookGtsStp() {
        return checkerTransactionsChequeBookGtsStp;
    }

    public void setCheckerTransactionsChequeBookGtsStp(int checkerTransactionsChequeBookGtsStp) {
        this.checkerTransactionsChequeBookGtsStp = checkerTransactionsChequeBookGtsStp;
    }

    public int getCheckerTransactionsTotalTransactionsRow() {
        return checkerTransactionsTotalTransactionsRow;
    }

    public void setCheckerTransactionsTotalTransactionsRow(int checkerTransactionsTotalTransactionsRow) {
        this.checkerTransactionsTotalTransactionsRow = checkerTransactionsTotalTransactionsRow;
    }

    public int getCheckerTransactionsMtd() {
        return checkerTransactionsMtd;
    }

    public void setCheckerTransactionsMtd(int checkerTransactionsMtd) {
        this.checkerTransactionsMtd = checkerTransactionsMtd;
    }

    public int getCheckerTransactionsPreviousMonth() {
        return checkerTransactionsPreviousMonth;
    }

    public void setCheckerTransactionsPreviousMonth(int checkerTransactionsPreviousMonth) {
        this.checkerTransactionsPreviousMonth = checkerTransactionsPreviousMonth;
    }
}
