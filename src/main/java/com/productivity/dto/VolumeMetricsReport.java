package com.productivity.dto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

//import static com.productivity.*;

public class VolumeMetricsReport {

    public static final Logger logger = LoggerFactory.getLogger(VolumeMetricsReport.class);

    private String totalEdmsString="total edms";
    private String totalTransactionsString="total transactions";


    //Todo: Add Total to maker and Checker Sets for accessing it from Thymeleaf
    Set<String> makerNameSet = new LinkedHashSet<>();
    Set<String> checkerNameSet = new LinkedHashSet<>();

    //Name,2 values for all Activities
    Map<String, Activities> productivityMakerCheckerMap = new HashMap<>();

    public String getTotalEdmsString() {
        return totalEdmsString;
    }

    public void setTotalEdmsString(String totalEdmsString) {
        this.totalEdmsString = totalEdmsString;
    }

    public String getTotalTransactionsString() {
        return totalTransactionsString;
    }

    public void setTotalTransactionsString(String totalTransactionsString) {
        this.totalTransactionsString = totalTransactionsString;
    }

    public Set<String> getMakerNameSet() {
        return makerNameSet;
    }

    public void setMakerNameSet(Set<String> makerNameSet) {
        this.makerNameSet = makerNameSet;
    }

    public Set<String> getCheckerNameSet() {
        return checkerNameSet;
    }

    public void setCheckerNameSet(Set<String> checkerNameSet) {
        this.checkerNameSet = checkerNameSet;
    }

    public Map<String, Activities> getProductivityMakerCheckerMap() {
        return productivityMakerCheckerMap;
    }

    public void setProductivityMakerCheckerMap(Map<String, Activities> productivityMakerCheckerMap) {
        this.productivityMakerCheckerMap = productivityMakerCheckerMap;
    }
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    public void addDataToProductivityMakerCheckerMap(ActivitiesEnums activityName, String name, int noOfEdms, int noOfTransactions)
    {
        Activities activities = productivityMakerCheckerMap.get(name);
        if(activities == null)
        {
            activities = new Activities();
            productivityMakerCheckerMap.put(name , activities);
        }
        //Total Transaction for Each Edms Columns in Maker table
        Activities totalNoOfEdmsForEachActivity = productivityMakerCheckerMap.get("total edms");
        if(totalNoOfEdmsForEachActivity == null)
        {
            totalNoOfEdmsForEachActivity = new Activities();
            productivityMakerCheckerMap.put("total edms" , totalNoOfEdmsForEachActivity);
        }
        //Total Transactions for Each column in makerTable
        Activities totalNoOfTransactionsForEachActivity = productivityMakerCheckerMap.get("total transactions");
        if(totalNoOfTransactionsForEachActivity == null)
        {
            totalNoOfTransactionsForEachActivity = new Activities();
            productivityMakerCheckerMap.put("total transactions" , totalNoOfTransactionsForEachActivity);
        }

        //TODO : Add data to particular property of productivity object passed as an argument to this method

//Todo: For All Activities in both Tables --> No.Of.Edms Column values setting below
        if(activityName.equals(ActivitiesEnums.MakerMtd)){
            activities.setMakerEdmsMtd(noOfEdms); totalNoOfEdmsForEachActivity.setMakerEdmsMtd(noOfEdms + totalNoOfEdmsForEachActivity.getMakerEdmsMtd());   }
        else if(activityName.equals(ActivitiesEnums.MakerPreviousMonth)){
            activities.setMakerEdmsPreviousMonth(noOfEdms); totalNoOfEdmsForEachActivity.setMakerEdmsPreviousMonth(noOfEdms + totalNoOfEdmsForEachActivity.getMakerEdmsPreviousMonth());  }
        else if(activityName.equals(ActivitiesEnums.MakerAccountOpeningFlex)){
            activities.setMakerEdmsAccountOpeningFlex(noOfEdms); totalNoOfEdmsForEachActivity.setMakerEdmsAccountOpeningFlex(noOfEdms + totalNoOfEdmsForEachActivity.getMakerEdmsAccountOpeningFlex());  }
        else if(activityName.equals(ActivitiesEnums.MakerAccountOpeningDob)){
            activities.setMakerEdmsAccountOpeningDob(noOfEdms); totalNoOfEdmsForEachActivity.setMakerEdmsAccountOpeningDob(noOfEdms + totalNoOfEdmsForEachActivity.getMakerEdmsAccountOpeningDob());  }
        else if(activityName.equals(ActivitiesEnums.MakerKycFlex)){
            activities.setMakerEdmsKycFlex(noOfEdms); totalNoOfEdmsForEachActivity.setMakerEdmsKycFlex(noOfEdms + totalNoOfEdmsForEachActivity.getMakerEdmsKycFlex());  }
        else if(activityName.equals(ActivitiesEnums.MakerKycBpm)){
            activities.setMakerEdmsKycBpm(noOfEdms); totalNoOfEdmsForEachActivity.setMakerEdmsKycBpm(noOfEdms + totalNoOfEdmsForEachActivity.getMakerEdmsKycBpm());  }
        else if(activityName.equals(ActivitiesEnums.MakerKycUnfreezeFlex)){
            activities.setMakerEdmsKycUnfreezeFlex(noOfEdms); totalNoOfEdmsForEachActivity.setMakerEdmsKycUnfreezeFlex(noOfEdms + totalNoOfEdmsForEachActivity.getMakerEdmsKycUnfreezeFlex());  }
        else if(activityName.equals(ActivitiesEnums.MakerAccountClosure)){
            activities.setMakerEdmsAccountClosure(noOfEdms); totalNoOfEdmsForEachActivity.setMakerEdmsAccountClosure(noOfEdms + totalNoOfEdmsForEachActivity.getMakerEdmsAccountClosure());  }
        else if(activityName.equals(ActivitiesEnums.MakerFixedDepositsForBreakageRenewals)){
            activities.setMakerEdmsFixedDepositsForBreakageRenewals(noOfEdms); totalNoOfEdmsForEachActivity.setMakerEdmsFixedDepositsForBreakageRenewals(noOfEdms + totalNoOfEdmsForEachActivity.getMakerEdmsFixedDepositsForBreakageRenewals());  }
        else if(activityName.equals(ActivitiesEnums.MakerFixedDepositsForGtsStp)){
            activities.setMakerEdmsFixedDepositsForGtsStp(noOfEdms); totalNoOfEdmsForEachActivity.setMakerEdmsFixedDepositsForGtsStp(noOfEdms + totalNoOfEdmsForEachActivity.getMakerEdmsFixedDepositsForGtsStp());  }
        else if(activityName.equals(ActivitiesEnums.MakerSignatureMandateAdditionOrAmmendment)){
            activities.setMakerEdmsSignatureMandateAdditionOrAmmendment(noOfEdms); totalNoOfEdmsForEachActivity.setMakerEdmsSignatureMandateAdditionOrAmmendment(noOfEdms + totalNoOfEdmsForEachActivity.getMakerEdmsSignatureMandateAdditionOrAmmendment());  }
        else if(activityName.equals(ActivitiesEnums.MakerTradeLicense)){
            activities.setMakerEdmsTradeLicense(noOfEdms); totalNoOfEdmsForEachActivity.setMakerEdmsTradeLicense(noOfEdms + totalNoOfEdmsForEachActivity.getMakerEdmsTradeLicense());  }
        else if(activityName.equals(ActivitiesEnums.MakerAccountMaintanenceMisc)){
            activities.setMakerEdmsAccountMaintanenceMisc(noOfEdms); totalNoOfEdmsForEachActivity.setMakerEdmsAccountMaintanenceMisc(noOfEdms + totalNoOfEdmsForEachActivity.getMakerEdmsAccountMaintanenceMisc());  }
        else if(activityName.equals(ActivitiesEnums.MakerChequeBookGtsStp)){
            activities.setMakerEdmsChequeBookGtsStp(noOfEdms); totalNoOfEdmsForEachActivity.setMakerEdmsChequeBookGtsStp(noOfEdms + totalNoOfEdmsForEachActivity.getMakerEdmsChequeBookGtsStp());  }
        //Todo: add here
        else if(activityName.equals(ActivitiesEnums.CheckerMtd)){
            activities.setCheckerEdmsMtd(noOfEdms); totalNoOfEdmsForEachActivity.setCheckerEdmsMtd(noOfEdms + totalNoOfEdmsForEachActivity.getCheckerEdmsMtd());   }
        else if(activityName.equals(ActivitiesEnums.CheckerPreviousMonth)){
            activities.setCheckerEdmsPreviousMonth(noOfEdms); totalNoOfEdmsForEachActivity.setCheckerEdmsPreviousMonth(noOfEdms + totalNoOfEdmsForEachActivity.getCheckerEdmsPreviousMonth());  }
        else if(activityName.equals(ActivitiesEnums.CheckerAccountOpeningFlex)){
            activities.setCheckerEdmsAccountOpeningFlex(noOfEdms); totalNoOfEdmsForEachActivity.setCheckerEdmsAccountOpeningFlex(noOfEdms + totalNoOfEdmsForEachActivity.getCheckerEdmsAccountOpeningFlex());  }
        else if(activityName.equals(ActivitiesEnums.CheckerAccountOpeningDob)){
            activities.setCheckerEdmsAccountOpeningDob(noOfEdms); totalNoOfEdmsForEachActivity.setCheckerEdmsAccountOpeningDob(noOfEdms + totalNoOfEdmsForEachActivity.getCheckerEdmsAccountOpeningDob());  }
        else if(activityName.equals(ActivitiesEnums.CheckerKycFlex)){
            activities.setCheckerEdmsKycFlex(noOfEdms); totalNoOfEdmsForEachActivity.setCheckerEdmsKycFlex(noOfEdms + totalNoOfEdmsForEachActivity.getCheckerEdmsKycFlex());  }
        else if(activityName.equals(ActivitiesEnums.CheckerKycBpm)){
            activities.setCheckerEdmsKycBpm(noOfEdms); totalNoOfEdmsForEachActivity.setCheckerEdmsKycBpm(noOfEdms + totalNoOfEdmsForEachActivity.getCheckerEdmsKycBpm());  }
        else if(activityName.equals(ActivitiesEnums.CheckerKycUnfreezeFlex)){
            activities.setCheckerEdmsKycUnfreezeFlex(noOfEdms); totalNoOfEdmsForEachActivity.setCheckerEdmsKycUnfreezeFlex(noOfEdms + totalNoOfEdmsForEachActivity.getCheckerEdmsKycUnfreezeFlex());  }
        else if(activityName.equals(ActivitiesEnums.CheckerAccountClosure)){
            activities.setCheckerEdmsAccountClosure(noOfEdms); totalNoOfEdmsForEachActivity.setCheckerEdmsAccountClosure(noOfEdms + totalNoOfEdmsForEachActivity.getCheckerEdmsAccountClosure());  }
        else if(activityName.equals(ActivitiesEnums.CheckerFixedDepositsForBreakageRenewals)){
            activities.setCheckerEdmsFixedDepositsForBreakageRenewals(noOfEdms); totalNoOfEdmsForEachActivity.setCheckerEdmsFixedDepositsForBreakageRenewals(noOfEdms + totalNoOfEdmsForEachActivity.getCheckerEdmsFixedDepositsForBreakageRenewals());  }
        else if(activityName.equals(ActivitiesEnums.CheckerFixedDepositsForGtsStp)){
            activities.setCheckerEdmsFixedDepositsForGtsStp(noOfEdms); totalNoOfEdmsForEachActivity.setCheckerEdmsFixedDepositsForGtsStp(noOfEdms + totalNoOfEdmsForEachActivity.getCheckerEdmsFixedDepositsForGtsStp());  }
        else if(activityName.equals(ActivitiesEnums.CheckerSignatureMandateAdditionOrAmmendment)){
            activities.setCheckerEdmsSignatureMandateAdditionOrAmmendment(noOfEdms); totalNoOfEdmsForEachActivity.setCheckerEdmsSignatureMandateAdditionOrAmmendment(noOfEdms + totalNoOfEdmsForEachActivity.getCheckerEdmsSignatureMandateAdditionOrAmmendment());  }
        else if(activityName.equals(ActivitiesEnums.CheckerTradeLicense)){
            activities.setCheckerEdmsTradeLicense(noOfEdms); totalNoOfEdmsForEachActivity.setCheckerEdmsTradeLicense(noOfEdms + totalNoOfEdmsForEachActivity.getCheckerEdmsTradeLicense());  }
        else if(activityName.equals(ActivitiesEnums.CheckerAccountMaintanenceMisc)){
            activities.setCheckerEdmsAccountMaintanenceMisc(noOfEdms); totalNoOfEdmsForEachActivity.setCheckerEdmsAccountMaintanenceMisc(noOfEdms + totalNoOfEdmsForEachActivity.getCheckerEdmsAccountMaintanenceMisc());  }
        else if(activityName.equals(ActivitiesEnums.CheckerChequeBookGtsStp)){
            activities.setCheckerEdmsChequeBookGtsStp(noOfEdms); totalNoOfEdmsForEachActivity.setCheckerEdmsChequeBookGtsStp(noOfEdms + totalNoOfEdmsForEachActivity.getCheckerEdmsChequeBookGtsStp());  }

        else{
            System.out.println("Activity for EDMS not available yet");
        }

        ///Todo://///////////////Maker/////////////////// Transactions //////////////////////////////////////////////////////
        //Todo: For All Activities in both Tables --> No.Of.Edms Column values setting below

        if(activityName.equals(ActivitiesEnums.MakerMtd)){
            activities.setMakerTransactionsMtd(noOfTransactions); totalNoOfTransactionsForEachActivity.setMakerTransactionsMtd(noOfTransactions + totalNoOfTransactionsForEachActivity.getMakerTransactionsMtd());   }
        else if(activityName.equals(ActivitiesEnums.MakerPreviousMonth)){
            activities.setMakerTransactionsPreviousMonth(noOfTransactions); totalNoOfTransactionsForEachActivity.setMakerTransactionsPreviousMonth(noOfTransactions + totalNoOfTransactionsForEachActivity.getMakerTransactionsPreviousMonth());  }
        else if(activityName.equals(ActivitiesEnums.MakerAccountOpeningFlex)){
            activities.setMakerTransactionsAccountOpeningFlex(noOfTransactions); totalNoOfTransactionsForEachActivity.setMakerTransactionsAccountOpeningFlex(noOfTransactions + totalNoOfTransactionsForEachActivity.getMakerTransactionsAccountOpeningFlex());  }
        else if(activityName.equals(ActivitiesEnums.MakerAccountOpeningDob)){
            activities.setMakerTransactionsAccountOpeningDob(noOfTransactions); totalNoOfTransactionsForEachActivity.setMakerTransactionsAccountOpeningDob(noOfTransactions + totalNoOfTransactionsForEachActivity.getMakerTransactionsAccountOpeningDob());  }
        else if(activityName.equals(ActivitiesEnums.MakerKycFlex)){
            activities.setMakerTransactionsKycFlex(noOfTransactions); totalNoOfTransactionsForEachActivity.setMakerTransactionsKycFlex(noOfTransactions + totalNoOfTransactionsForEachActivity.getMakerTransactionsKycFlex());  }
        else if(activityName.equals(ActivitiesEnums.MakerKycBpm)){
            activities.setMakerTransactionsKycBpm(noOfTransactions); totalNoOfTransactionsForEachActivity.setMakerTransactionsKycBpm(noOfTransactions + totalNoOfTransactionsForEachActivity.getMakerTransactionsKycBpm());  }
        else if(activityName.equals(ActivitiesEnums.MakerKycUnfreezeFlex)){
            activities.setMakerTransactionsKycUnfreezeFlex(noOfTransactions); totalNoOfTransactionsForEachActivity.setMakerTransactionsKycUnfreezeFlex(noOfTransactions + totalNoOfTransactionsForEachActivity.getMakerTransactionsKycUnfreezeFlex());  }
        else if(activityName.equals(ActivitiesEnums.MakerAccountClosure)){
            activities.setMakerTransactionsAccountClosure(noOfTransactions); totalNoOfTransactionsForEachActivity.setMakerTransactionsAccountClosure(noOfTransactions + totalNoOfTransactionsForEachActivity.getMakerTransactionsAccountClosure());  }
        else if(activityName.equals(ActivitiesEnums.MakerFixedDepositsForBreakageRenewals)){
            activities.setMakerTransactionsFixedDepositsForBreakageRenewals(noOfTransactions); totalNoOfTransactionsForEachActivity.setMakerTransactionsFixedDepositsForBreakageRenewals(noOfTransactions + totalNoOfTransactionsForEachActivity.getMakerTransactionsFixedDepositsForBreakageRenewals());  }
        else if(activityName.equals(ActivitiesEnums.MakerFixedDepositsForGtsStp)){
            activities.setMakerTransactionsFixedDepositsForGtsStp(noOfTransactions); totalNoOfTransactionsForEachActivity.setMakerTransactionsFixedDepositsForGtsStp(noOfTransactions + totalNoOfTransactionsForEachActivity.getMakerTransactionsFixedDepositsForGtsStp());  }
        else if(activityName.equals(ActivitiesEnums.MakerSignatureMandateAdditionOrAmmendment)){
            activities.setMakerTransactionsSignatureMandateAdditionOrAmmendment(noOfTransactions); totalNoOfTransactionsForEachActivity.setMakerTransactionsSignatureMandateAdditionOrAmmendment(noOfTransactions + totalNoOfTransactionsForEachActivity.getMakerTransactionsSignatureMandateAdditionOrAmmendment());  }
        else if(activityName.equals(ActivitiesEnums.MakerTradeLicense)){
            activities.setMakerTransactionsTradeLicense(noOfTransactions); totalNoOfTransactionsForEachActivity.setMakerTransactionsTradeLicense(noOfTransactions + totalNoOfTransactionsForEachActivity.getMakerTransactionsTradeLicense());  }
        else if(activityName.equals(ActivitiesEnums.MakerAccountMaintanenceMisc)){
            activities.setMakerTransactionsAccountMaintanenceMisc(noOfTransactions); totalNoOfTransactionsForEachActivity.setMakerTransactionsAccountMaintanenceMisc(noOfTransactions + totalNoOfTransactionsForEachActivity.getMakerTransactionsAccountMaintanenceMisc());  }
        else if(activityName.equals(ActivitiesEnums.MakerChequeBookGtsStp)){
            activities.setMakerTransactionsChequeBookGtsStp(noOfTransactions); totalNoOfTransactionsForEachActivity.setMakerTransactionsChequeBookGtsStp(noOfTransactions + totalNoOfTransactionsForEachActivity.getMakerTransactionsChequeBookGtsStp());  }
//Todo: add here
        else if(activityName.equals(ActivitiesEnums.CheckerMtd)){
            activities.setCheckerTransactionsMtd(noOfTransactions); totalNoOfTransactionsForEachActivity.setCheckerTransactionsMtd(noOfTransactions + totalNoOfTransactionsForEachActivity.getCheckerTransactionsMtd());   }
        else if(activityName.equals(ActivitiesEnums.CheckerPreviousMonth)){
            activities.setCheckerTransactionsPreviousMonth(noOfTransactions); totalNoOfTransactionsForEachActivity.setCheckerTransactionsPreviousMonth(noOfTransactions + totalNoOfTransactionsForEachActivity.getCheckerTransactionsPreviousMonth());  }
        else if(activityName.equals(ActivitiesEnums.CheckerAccountOpeningFlex)){
            activities.setCheckerTransactionsAccountOpeningFlex(noOfTransactions); totalNoOfTransactionsForEachActivity.setCheckerTransactionsAccountOpeningFlex(noOfTransactions + totalNoOfTransactionsForEachActivity.getCheckerTransactionsAccountOpeningFlex());  }
        else if(activityName.equals(ActivitiesEnums.CheckerAccountOpeningDob)){
            activities.setCheckerTransactionsAccountOpeningDob(noOfTransactions); totalNoOfTransactionsForEachActivity.setCheckerTransactionsAccountOpeningDob(noOfTransactions + totalNoOfTransactionsForEachActivity.getCheckerTransactionsAccountOpeningDob());  }
        else if(activityName.equals(ActivitiesEnums.CheckerKycFlex)){
            activities.setCheckerTransactionsKycFlex(noOfTransactions); totalNoOfTransactionsForEachActivity.setCheckerTransactionsKycFlex(noOfTransactions + totalNoOfTransactionsForEachActivity.getCheckerTransactionsKycFlex());  }
        else if(activityName.equals(ActivitiesEnums.CheckerKycBpm)){
            activities.setCheckerTransactionsKycBpm(noOfTransactions); totalNoOfTransactionsForEachActivity.setCheckerTransactionsKycBpm(noOfTransactions + totalNoOfTransactionsForEachActivity.getCheckerTransactionsKycBpm());  }
        else if(activityName.equals(ActivitiesEnums.CheckerKycUnfreezeFlex)){
            activities.setCheckerTransactionsKycUnfreezeFlex(noOfTransactions); totalNoOfTransactionsForEachActivity.setCheckerTransactionsKycUnfreezeFlex(noOfTransactions + totalNoOfTransactionsForEachActivity.getCheckerTransactionsKycUnfreezeFlex());  }
        else if(activityName.equals(ActivitiesEnums.CheckerAccountClosure)){
            activities.setCheckerTransactionsAccountClosure(noOfTransactions); totalNoOfTransactionsForEachActivity.setCheckerTransactionsAccountClosure(noOfTransactions + totalNoOfTransactionsForEachActivity.getCheckerTransactionsAccountClosure());  }
        else if(activityName.equals(ActivitiesEnums.CheckerFixedDepositsForBreakageRenewals)){
            activities.setCheckerTransactionsFixedDepositsForBreakageRenewals(noOfTransactions); totalNoOfTransactionsForEachActivity.setCheckerTransactionsFixedDepositsForBreakageRenewals(noOfTransactions + totalNoOfTransactionsForEachActivity.getCheckerTransactionsFixedDepositsForBreakageRenewals());  }
        else if(activityName.equals(ActivitiesEnums.CheckerFixedDepositsForGtsStp)){
            activities.setCheckerTransactionsFixedDepositsForGtsStp(noOfTransactions); totalNoOfTransactionsForEachActivity.setCheckerTransactionsFixedDepositsForGtsStp(noOfTransactions + totalNoOfTransactionsForEachActivity.getCheckerTransactionsFixedDepositsForGtsStp());  }
        else if(activityName.equals(ActivitiesEnums.CheckerSignatureMandateAdditionOrAmmendment)){
            activities.setCheckerTransactionsSignatureMandateAdditionOrAmmendment(noOfTransactions); totalNoOfTransactionsForEachActivity.setCheckerTransactionsSignatureMandateAdditionOrAmmendment(noOfTransactions + totalNoOfTransactionsForEachActivity.getCheckerTransactionsSignatureMandateAdditionOrAmmendment());  }
        else if(activityName.equals(ActivitiesEnums.CheckerTradeLicense)){
            activities.setCheckerTransactionsTradeLicense(noOfTransactions); totalNoOfTransactionsForEachActivity.setCheckerTransactionsTradeLicense(noOfTransactions + totalNoOfTransactionsForEachActivity.getCheckerTransactionsTradeLicense());  }
        else if(activityName.equals(ActivitiesEnums.CheckerAccountMaintanenceMisc)){
            activities.setCheckerTransactionsAccountMaintanenceMisc(noOfTransactions); totalNoOfTransactionsForEachActivity.setCheckerTransactionsAccountMaintanenceMisc(noOfTransactions + totalNoOfTransactionsForEachActivity.getCheckerTransactionsAccountMaintanenceMisc());  }
        else if(activityName.equals(ActivitiesEnums.CheckerChequeBookGtsStp)){
            activities.setCheckerTransactionsChequeBookGtsStp(noOfTransactions); totalNoOfTransactionsForEachActivity.setCheckerTransactionsChequeBookGtsStp(noOfTransactions + totalNoOfTransactionsForEachActivity.getCheckerTransactionsChequeBookGtsStp());  }
        else{
            System.out.println("Activity for Transactions not available yet");
        }







    }

    public void calculateTotalRow()
    {

        Set<String> makerNameSetWithTotaOfEdmsAndTransactions = new LinkedHashSet<>();
        makerNameSetWithTotaOfEdmsAndTransactions.addAll(makerNameSet);
        makerNameSetWithTotaOfEdmsAndTransactions.add("total edms");
        makerNameSetWithTotaOfEdmsAndTransactions.add("total transactions");

       for(String makerName : makerNameSetWithTotaOfEdmsAndTransactions)
       {
           Activities activities = productivityMakerCheckerMap.get(makerName);
           int totalEdmsMaker = activities.getMakerEdmsAccountOpeningFlex() +
                   activities.getMakerEdmsAccountOpeningDob() +
                   activities.getMakerEdmsKycFlex() +
                   activities.getMakerEdmsKycBpm() +
                   activities.getMakerEdmsKycUnfreezeFlex() +
                   activities.getMakerEdmsAccountClosure() +
                   activities.getMakerEdmsFixedDepositsForBreakageRenewals() +
                   activities.getMakerEdmsFixedDepositsForGtsStp() +
                   activities.getMakerEdmsSignatureMandateAdditionOrAmmendment() +
                   activities.getMakerEdmsTradeLicense() +
                   activities.getMakerEdmsAccountMaintanenceMisc() +
                   activities.getMakerEdmsChequeBookGtsStp() ;
           activities.setMakerEdmsTotalTransactionsRow(totalEdmsMaker);

           int totalTransactionsMaker = activities.getMakerTransactionsAccountOpeningFlex() +
                   activities.getMakerTransactionsAccountOpeningDob() +
                   activities.getMakerTransactionsKycFlex() +
                   activities.getMakerTransactionsKycBpm() +
                   activities.getMakerTransactionsKycUnfreezeFlex() +
                   activities.getMakerTransactionsAccountClosure() +
                   activities.getMakerTransactionsFixedDepositsForBreakageRenewals() +
                   activities.getMakerTransactionsFixedDepositsForGtsStp() +
                   activities.getMakerTransactionsSignatureMandateAdditionOrAmmendment() +
                   activities.getMakerTransactionsTradeLicense() +
                   activities.getMakerTransactionsAccountMaintanenceMisc() +
                   activities.getMakerTransactionsChequeBookGtsStp();
           activities.setMakerTransactionsTotalTransactionsRow(totalTransactionsMaker);

       }
       //ForCheckerTable
        Set<String> checkerNameSetWithTotaOfEdmsAndTransactions = new LinkedHashSet<>();
        checkerNameSetWithTotaOfEdmsAndTransactions.addAll(checkerNameSet);
        checkerNameSetWithTotaOfEdmsAndTransactions.add("total edms");
        checkerNameSetWithTotaOfEdmsAndTransactions.add("total transactions");
        for(String checkerName : checkerNameSetWithTotaOfEdmsAndTransactions)
        {
            Activities activities = productivityMakerCheckerMap.get(checkerName);
            int totalEdmsChecker = activities.getCheckerEdmsAccountOpeningFlex() +
                    activities.getCheckerEdmsAccountOpeningDob() +
                    activities.getCheckerEdmsKycFlex() +
                    activities.getCheckerEdmsKycBpm() +
                    activities.getCheckerEdmsKycUnfreezeFlex() +
                    activities.getCheckerEdmsAccountClosure() +
                    activities.getCheckerEdmsFixedDepositsForBreakageRenewals() +
                    activities.getCheckerEdmsFixedDepositsForGtsStp() +
                    activities.getCheckerEdmsSignatureMandateAdditionOrAmmendment() +
                    activities.getCheckerEdmsTradeLicense() +
                    activities.getCheckerEdmsAccountMaintanenceMisc() +
                    activities.getCheckerEdmsChequeBookGtsStp() ;
            activities.setCheckerEdmsTotalTransactionsRow(totalEdmsChecker);

            int totalTransactionsChecker = activities.getCheckerTransactionsAccountOpeningFlex() +
                    activities.getCheckerTransactionsAccountOpeningDob() +
                    activities.getCheckerTransactionsKycFlex() +
                    activities.getCheckerTransactionsKycBpm() +
                    activities.getCheckerTransactionsKycUnfreezeFlex() +
                    activities.getCheckerTransactionsAccountClosure() +
                    activities.getCheckerTransactionsFixedDepositsForBreakageRenewals() +
                    activities.getCheckerTransactionsFixedDepositsForGtsStp() +
                    activities.getCheckerTransactionsSignatureMandateAdditionOrAmmendment() +
                    activities.getCheckerTransactionsTradeLicense() +
                    activities.getCheckerTransactionsAccountMaintanenceMisc() +
                    activities.getCheckerTransactionsChequeBookGtsStp();
            activities.setCheckerTransactionsTotalTransactionsRow(totalTransactionsChecker);
        }





    }

    public String getString(int value)
    {
        if(value == 0)
        {
            return "-";
        }else{
            return ""+ value;
        }
    }



}
