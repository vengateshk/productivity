package com.productivity.dto;

public enum ActivitiesEnums {
    Maker,
    MakerAccountOpeningFlex,
    MakerAccountOpeningDob,
    MakerKycFlex,
    MakerKycBpm,
    MakerKycUnfreezeFlex,
    MakerAccountClosure,
    MakerFixedDepositsForBreakageRenewals,
    MakerFixedDepositsForGtsStp,
    MakerSignatureMandateAdditionOrAmmendment,
    MakerTradeLicense,
    MakerAccountMaintanenceMisc,
    MakerChequeBookGtsStp,
    MakerTotal,
    MakerMtd,
    MakerPreviousMonth,


    Checker,
    CheckerAccountOpeningFlex,
    CheckerAccountOpeningDob,
    CheckerKycFlex,
    CheckerKycBpm,
    CheckerKycUnfreezeFlex,
    CheckerAccountClosure,
    CheckerFixedDepositsForBreakageRenewals,
    CheckerFixedDepositsForGtsStp,
    CheckerSignatureMandateAdditionOrAmmendment,
    CheckerTradeLicense,
    CheckerAccountMaintanenceMisc,
    CheckerChequeBookGtsStp,
    CheckerMtd,
    CheckerPreviousMonth


}
