package com.productivity.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "hive.query")
public class QueriesConfig {

    private String excelDumpFileLocationOne;
    private String excelDumpFileLocationTwo;

    //Todo: Add only HTMLBody Queries here for Maker And Checker....
    //Maker Table Queries
    private String queryForMakerMtd;
    private String queryForMakerPreviousMonth;
    private String queryForMakerAccountOpeningFlex;
    private String queryForMakerAccountOpeningDob;
    private String queryForMakerKycFlex;
    private String queryForMakerKycBpm;
    private String queryForMakerKycUnfreezeFlex;
    private String queryForMakerAccountClosure;
    private String queryForMakerFixedDepositsForBreakageRenewals;
    private String queryForMakerFixedDepositsForGtsStp;
    private String queryForMakerSignatureMandateAdditionOrAmmendment;
    private String queryForMakerTradeLicense;
    private String queryForMakerAccountMaintanenceMisc;
    private String queryForMakerChequeBookGtsStp;

    //Checker Table Queris
    private String queryForCheckerMtd;
    private String queryForCheckerPreviousMonth;
    private String queryForCheckerAccountOpeningFlex;
    private String queryForCheckerAccountOpeningDob;
    private String queryForCheckerKycFlex;
    private String queryForCheckerKycBpm;
    private String queryForCheckerKycUnfreezeFlex;
    private String queryForCheckerAccountClosure;
    private String queryForCheckerFixedDepositsForBreakageRenewals;
    private String queryForCheckerFixedDepositsForGtsStp;
    private String queryForCheckerSignatureMandateAdditionOrAmmendment;
    private String queryForCheckerTradeLicense;
    private String queryForCheckerAccountMaintanenceMisc;
    private String queryForCheckerChequeBookGtsStp;


    public String getExcelDumpFileLocationTwo() {
        return excelDumpFileLocationTwo;
    }

    public void setExcelDumpFileLocationTwo(String excelDumpFileLocationTwo) {
        this.excelDumpFileLocationTwo = excelDumpFileLocationTwo;
    }

    public String getExcelDumpFileLocationOne() {
        return excelDumpFileLocationOne;
    }

    public void setExcelDumpFileLocationOne(String excelDumpFileLocationOne) {
        this.excelDumpFileLocationOne = excelDumpFileLocationOne;
    }

    public String getQueryForMakerMtd() {
        return queryForMakerMtd;
    }

    public void setQueryForMakerMtd(String queryForMakerMtd) {
        this.queryForMakerMtd = queryForMakerMtd;
    }

    public String getQueryForMakerPreviousMonth() {
        return queryForMakerPreviousMonth;
    }

    public void setQueryForMakerPreviousMonth(String queryForMakerPreviousMonth) {
        this.queryForMakerPreviousMonth = queryForMakerPreviousMonth;
    }

    public String getQueryForMakerAccountOpeningFlex() {
        return queryForMakerAccountOpeningFlex;
    }

    public void setQueryForMakerAccountOpeningFlex(String queryForMakerAccountOpeningFlex) {
        this.queryForMakerAccountOpeningFlex = queryForMakerAccountOpeningFlex;
    }

    public String getQueryForMakerAccountOpeningDob() {
        return queryForMakerAccountOpeningDob;
    }

    public void setQueryForMakerAccountOpeningDob(String queryForMakerAccountOpeningDob) {
        this.queryForMakerAccountOpeningDob = queryForMakerAccountOpeningDob;
    }

    public String getQueryForMakerKycFlex() {
        return queryForMakerKycFlex;
    }

    public void setQueryForMakerKycFlex(String queryForMakerKycFlex) {
        this.queryForMakerKycFlex = queryForMakerKycFlex;
    }

    public String getQueryForMakerKycBpm() {
        return queryForMakerKycBpm;
    }

    public void setQueryForMakerKycBpm(String queryForMakerKycBpm) {
        this.queryForMakerKycBpm = queryForMakerKycBpm;
    }

    public String getQueryForMakerKycUnfreezeFlex() {
        return queryForMakerKycUnfreezeFlex;
    }

    public void setQueryForMakerKycUnfreezeFlex(String queryForMakerKycUnfreezeFlex) {
        this.queryForMakerKycUnfreezeFlex = queryForMakerKycUnfreezeFlex;
    }

    public String getQueryForMakerAccountClosure() {
        return queryForMakerAccountClosure;
    }

    public void setQueryForMakerAccountClosure(String queryForMakerAccountClosure) {
        this.queryForMakerAccountClosure = queryForMakerAccountClosure;
    }

    public String getQueryForMakerFixedDepositsForBreakageRenewals() {
        return queryForMakerFixedDepositsForBreakageRenewals;
    }

    public void setQueryForMakerFixedDepositsForBreakageRenewals(String queryForMakerFixedDepositsForBreakageRenewals) {
        this.queryForMakerFixedDepositsForBreakageRenewals = queryForMakerFixedDepositsForBreakageRenewals;
    }

    public String getQueryForMakerFixedDepositsForGtsStp() {
        return queryForMakerFixedDepositsForGtsStp;
    }

    public void setQueryForMakerFixedDepositsForGtsStp(String queryForMakerFixedDepositsForGtsStp) {
        this.queryForMakerFixedDepositsForGtsStp = queryForMakerFixedDepositsForGtsStp;
    }

    public String getQueryForMakerSignatureMandateAdditionOrAmmendment() {
        return queryForMakerSignatureMandateAdditionOrAmmendment;
    }

    public void setQueryForMakerSignatureMandateAdditionOrAmmendment(String queryForMakerSignatureMandateAdditionOrAmmendment) {
        this.queryForMakerSignatureMandateAdditionOrAmmendment = queryForMakerSignatureMandateAdditionOrAmmendment;
    }

    public String getQueryForMakerTradeLicense() {
        return queryForMakerTradeLicense;
    }

    public void setQueryForMakerTradeLicense(String queryForMakerTradeLicense) {
        this.queryForMakerTradeLicense = queryForMakerTradeLicense;
    }

    public String getQueryForMakerAccountMaintanenceMisc() {
        return queryForMakerAccountMaintanenceMisc;
    }

    public void setQueryForMakerAccountMaintanenceMisc(String queryForMakerAccountMaintanenceMisc) {
        this.queryForMakerAccountMaintanenceMisc = queryForMakerAccountMaintanenceMisc;
    }

    public String getQueryForMakerChequeBookGtsStp() {
        return queryForMakerChequeBookGtsStp;
    }

    public void setQueryForMakerChequeBookGtsStp(String queryForMakerChequeBookGtsStp) {
        this.queryForMakerChequeBookGtsStp = queryForMakerChequeBookGtsStp;
    }

    public String getQueryForCheckerMtd() {
        return queryForCheckerMtd;
    }

    public void setQueryForCheckerMtd(String queryForCheckerMtd) {
        this.queryForCheckerMtd = queryForCheckerMtd;
    }

    public String getQueryForCheckerPreviousMonth() {
        return queryForCheckerPreviousMonth;
    }

    public void setQueryForCheckerPreviousMonth(String queryForCheckerPreviousMonth) {
        this.queryForCheckerPreviousMonth = queryForCheckerPreviousMonth;
    }

    public String getQueryForCheckerAccountOpeningFlex() {
        return queryForCheckerAccountOpeningFlex;
    }

    public void setQueryForCheckerAccountOpeningFlex(String queryForCheckerAccountOpeningFlex) {
        this.queryForCheckerAccountOpeningFlex = queryForCheckerAccountOpeningFlex;
    }

    public String getQueryForCheckerAccountOpeningDob() {
        return queryForCheckerAccountOpeningDob;
    }

    public void setQueryForCheckerAccountOpeningDob(String queryForCheckerAccountOpeningDob) {
        this.queryForCheckerAccountOpeningDob = queryForCheckerAccountOpeningDob;
    }

    public String getQueryForCheckerKycFlex() {
        return queryForCheckerKycFlex;
    }

    public void setQueryForCheckerKycFlex(String queryForCheckerKycFlex) {
        this.queryForCheckerKycFlex = queryForCheckerKycFlex;
    }

    public String getQueryForCheckerKycBpm() {
        return queryForCheckerKycBpm;
    }

    public void setQueryForCheckerKycBpm(String queryForCheckerKycBpm) {
        this.queryForCheckerKycBpm = queryForCheckerKycBpm;
    }

    public String getQueryForCheckerKycUnfreezeFlex() {
        return queryForCheckerKycUnfreezeFlex;
    }

    public void setQueryForCheckerKycUnfreezeFlex(String queryForCheckerKycUnfreezeFlex) {
        this.queryForCheckerKycUnfreezeFlex = queryForCheckerKycUnfreezeFlex;
    }

    public String getQueryForCheckerAccountClosure() {
        return queryForCheckerAccountClosure;
    }

    public void setQueryForCheckerAccountClosure(String queryForCheckerAccountClosure) {
        this.queryForCheckerAccountClosure = queryForCheckerAccountClosure;
    }

    public String getQueryForCheckerFixedDepositsForBreakageRenewals() {
        return queryForCheckerFixedDepositsForBreakageRenewals;
    }

    public void setQueryForCheckerFixedDepositsForBreakageRenewals(String queryForCheckerFixedDepositsForBreakageRenewals) {
        this.queryForCheckerFixedDepositsForBreakageRenewals = queryForCheckerFixedDepositsForBreakageRenewals;
    }

    public String getQueryForCheckerFixedDepositsForGtsStp() {
        return queryForCheckerFixedDepositsForGtsStp;
    }

    public void setQueryForCheckerFixedDepositsForGtsStp(String queryForCheckerFixedDepositsForGtsStp) {
        this.queryForCheckerFixedDepositsForGtsStp = queryForCheckerFixedDepositsForGtsStp;
    }

    public String getQueryForCheckerSignatureMandateAdditionOrAmmendment() {
        return queryForCheckerSignatureMandateAdditionOrAmmendment;
    }

    public void setQueryForCheckerSignatureMandateAdditionOrAmmendment(String queryForCheckerSignatureMandateAdditionOrAmmendment) {
        this.queryForCheckerSignatureMandateAdditionOrAmmendment = queryForCheckerSignatureMandateAdditionOrAmmendment;
    }

    public String getQueryForCheckerTradeLicense() {
        return queryForCheckerTradeLicense;
    }

    public void setQueryForCheckerTradeLicense(String queryForCheckerTradeLicense) {
        this.queryForCheckerTradeLicense = queryForCheckerTradeLicense;
    }

    public String getQueryForCheckerAccountMaintanenceMisc() {
        return queryForCheckerAccountMaintanenceMisc;
    }

    public void setQueryForCheckerAccountMaintanenceMisc(String queryForCheckerAccountMaintanenceMisc) {
        this.queryForCheckerAccountMaintanenceMisc = queryForCheckerAccountMaintanenceMisc;
    }

    public String getQueryForCheckerChequeBookGtsStp() {
        return queryForCheckerChequeBookGtsStp;
    }

    public void setQueryForCheckerChequeBookGtsStp(String queryForCheckerChequeBookGtsStp) {
        this.queryForCheckerChequeBookGtsStp = queryForCheckerChequeBookGtsStp;
    }
}