package com.productivity.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;


@Configuration
@ConfigurationProperties(prefix = "conf")
public class ConfigurationHelper {


    private String excelFileLocationCheker;
    private String excelFileLocationMaker;

    //Todo: Add sheetNames if required

    public String getExcelFileLocationCheker() {
        return excelFileLocationCheker;
    }

    public void setExcelFileLocationCheker(String excelFileLocationCheker) {
        this.excelFileLocationCheker = excelFileLocationCheker;
    }

    public String getExcelFileLocationMaker() {
        return excelFileLocationMaker;
    }

    public void setExcelFileLocationMaker(String excelFileLocationMaker) {
        this.excelFileLocationMaker = excelFileLocationMaker;
    }

    @Override
    public String toString() {
        return "ConfigurationHelper{" +
                "excelFileLocationCheker='" + excelFileLocationCheker + '\'' +
                ", excelFileLocationMaker='" + excelFileLocationMaker + '\'' +
                '}';
    }
}
