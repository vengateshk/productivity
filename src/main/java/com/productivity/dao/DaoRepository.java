package com.productivity.dao;

import com.productivity.config.QueriesConfig;
import com.productivity.dto.ActivitiesEnums;
import com.productivity.dto.VolumeMetricsReport;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Repository
public class DaoRepository {

    public static final Logger logger = LoggerFactory.getLogger(DaoRepository.class);

    private JdbcTemplate jdbcTemplate;

    private QueriesConfig queriesConfig;


    @Autowired
    public DaoRepository(@Qualifier("hiveJdbcTemplate") JdbcTemplate jdbcTemplate, QueriesConfig queriesConfig) {

        this.jdbcTemplate = jdbcTemplate;
        this.queriesConfig = queriesConfig;

    }

    private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public String formateDate(java.sql.Date date) {

        String text = dateFormat.format(date);
        return text;
    }

    public void getDataForMakerAndCheckerTable(VolumeMetricsReport volumeMetricsReport,Date businessDate,Date nextBusinessDate, Date thisMonthStart, Date previousMonthStart, Date previousMonthEnd) {

        ////Multiple Queries for fetching
        int queryCount=1 ;
        System.out.println("Maker Table Queries started running");
//Maker Table Queries
        //todo : pass -->querycount , MakerOrChecker , 2 dates ,
        getDataFromDbAndAddItToMap(ActivitiesEnums.MakerMtd,queriesConfig.getQueryForMakerMtd(),volumeMetricsReport,queryCount++,ActivitiesEnums.Maker,thisMonthStart,businessDate); //<=
        getDataFromDbAndAddItToMap(ActivitiesEnums.MakerPreviousMonth, queriesConfig.getQueryForMakerPreviousMonth(),volumeMetricsReport,queryCount++,ActivitiesEnums.Maker,previousMonthStart,previousMonthEnd);//<=


        getDataFromDbAndAddItToMap(ActivitiesEnums.MakerAccountOpeningFlex, queriesConfig.getQueryForMakerAccountOpeningFlex(),volumeMetricsReport,queryCount++,ActivitiesEnums.Maker,businessDate,nextBusinessDate);
        //getDataFromDbAndAddItToMap(ActivitiesEnums.MakerAccountOpeningDob , queriesConfig.getQueryForMakerAccountOpeningDob(),volumeMetricsReport,queryCount++,ActivitiesEnums.Maker,businessDate,nextBusinessDate);
        getDataFromDbAndAddItToMap(ActivitiesEnums.MakerKycFlex , queriesConfig.getQueryForMakerKycFlex(),volumeMetricsReport,queryCount++,ActivitiesEnums.Maker,businessDate,nextBusinessDate);
        //getDataFromDbAndAddItToMap(ActivitiesEnums.MakerKycBpm , queriesConfig.getQueryForMakerKycBpm(),volumeMetricsReport,queryCount++,ActivitiesEnums.Maker,businessDate,nextBusinessDate);
        getDataFromDbAndAddItToMap(ActivitiesEnums.MakerKycUnfreezeFlex , queriesConfig.getQueryForMakerKycUnfreezeFlex(),volumeMetricsReport,queryCount++,ActivitiesEnums.Maker,businessDate,nextBusinessDate);
        getDataFromDbAndAddItToMap(ActivitiesEnums.MakerAccountClosure , queriesConfig.getQueryForMakerAccountClosure(),volumeMetricsReport,queryCount++,ActivitiesEnums.Maker,businessDate,nextBusinessDate);
        getDataFromDbAndAddItToMap(ActivitiesEnums.MakerFixedDepositsForBreakageRenewals , queriesConfig.getQueryForMakerFixedDepositsForBreakageRenewals(),volumeMetricsReport,queryCount++,ActivitiesEnums.Maker,businessDate,nextBusinessDate);
     //   getDataFromDbAndAddItToMap(ActivitiesEnums.MakerFixedDepositsForGtsStp , queriesConfig.getQueryForMakerFixedDepositsForGtsStp(),volumeMetricsReport,queryCount++,ActivitiesEnums.Maker,businessDate,nextBusinessDate);
        getDataFromDbAndAddItToMap(ActivitiesEnums.MakerSignatureMandateAdditionOrAmmendment , queriesConfig.getQueryForMakerSignatureMandateAdditionOrAmmendment(),volumeMetricsReport,queryCount++,ActivitiesEnums.Maker,businessDate,nextBusinessDate);
        getDataFromDbAndAddItToMap(ActivitiesEnums.MakerTradeLicense , queriesConfig.getQueryForMakerTradeLicense(),volumeMetricsReport,queryCount++,ActivitiesEnums.Maker,businessDate,nextBusinessDate);
        getDataFromDbAndAddItToMap(ActivitiesEnums.MakerAccountMaintanenceMisc , queriesConfig.getQueryForMakerAccountMaintanenceMisc(),volumeMetricsReport,queryCount++,ActivitiesEnums.Maker,businessDate,nextBusinessDate);
      //  getDataFromDbAndAddItToMap(ActivitiesEnums.MakerChequeBookGtsStp , queriesConfig.getQueryForMakerChequeBookGtsStp(),volumeMetricsReport,queryCount++,ActivitiesEnums.Maker,businessDate,nextBusinessDate);


        System.out.println("=====Maker Table Queries Executed====");
//Checker Table Queries
        System.out.println("Checker Table Queries started running");
        getDataFromDbAndAddItToMap(ActivitiesEnums.CheckerMtd,queriesConfig.getQueryForCheckerMtd(),volumeMetricsReport,queryCount++,ActivitiesEnums.Checker,thisMonthStart,businessDate); //<=
        getDataFromDbAndAddItToMap(ActivitiesEnums.CheckerPreviousMonth, queriesConfig.getQueryForCheckerPreviousMonth(),volumeMetricsReport,queryCount++,ActivitiesEnums.Checker,previousMonthStart,previousMonthEnd);//<=

        getDataFromDbAndAddItToMap(ActivitiesEnums.CheckerAccountOpeningFlex, queriesConfig.getQueryForCheckerAccountOpeningFlex(),volumeMetricsReport,queryCount++,ActivitiesEnums.Checker,businessDate,nextBusinessDate);
        //getDataFromDbAndAddItToMap(ActivitiesEnums.CheckerAccountOpeningDob , queriesConfig.getQueryForCheckerAccountOpeningDob(),volumeMetricsReport,queryCount++,ActivitiesEnums.Checker,businessDate,nextBusinessDate);
        getDataFromDbAndAddItToMap(ActivitiesEnums.CheckerKycFlex , queriesConfig.getQueryForCheckerKycFlex(),volumeMetricsReport,queryCount++,ActivitiesEnums.Checker,businessDate,nextBusinessDate);
      //  getDataFromDbAndAddItToMap(ActivitiesEnums.CheckerKycBpm , queriesConfig.getQueryForCheckerKycBpm(),volumeMetricsReport,queryCount++,ActivitiesEnums.Checker,businessDate,nextBusinessDate);
        getDataFromDbAndAddItToMap(ActivitiesEnums.CheckerKycUnfreezeFlex , queriesConfig.getQueryForCheckerKycUnfreezeFlex(),volumeMetricsReport,queryCount++,ActivitiesEnums.Checker,businessDate,nextBusinessDate);
        getDataFromDbAndAddItToMap(ActivitiesEnums.CheckerAccountClosure , queriesConfig.getQueryForCheckerAccountClosure(),volumeMetricsReport,queryCount++,ActivitiesEnums.Checker,businessDate,nextBusinessDate);
        getDataFromDbAndAddItToMap(ActivitiesEnums.CheckerFixedDepositsForBreakageRenewals , queriesConfig.getQueryForCheckerFixedDepositsForBreakageRenewals(),volumeMetricsReport,queryCount++,ActivitiesEnums.Checker,businessDate,nextBusinessDate);
      //  getDataFromDbAndAddItToMap(ActivitiesEnums.CheckerFixedDepositsForGtsStp , queriesConfig.getQueryForCheckerFixedDepositsForGtsStp(),volumeMetricsReport,queryCount++,ActivitiesEnums.Checker,businessDate,nextBusinessDate);
        getDataFromDbAndAddItToMap(ActivitiesEnums.CheckerSignatureMandateAdditionOrAmmendment , queriesConfig.getQueryForCheckerSignatureMandateAdditionOrAmmendment(),volumeMetricsReport,queryCount++,ActivitiesEnums.Checker,businessDate,nextBusinessDate);
        getDataFromDbAndAddItToMap(ActivitiesEnums.CheckerTradeLicense , queriesConfig.getQueryForCheckerTradeLicense(),volumeMetricsReport,queryCount++,ActivitiesEnums.Checker,businessDate,nextBusinessDate);
        getDataFromDbAndAddItToMap(ActivitiesEnums.CheckerAccountMaintanenceMisc , queriesConfig.getQueryForCheckerAccountMaintanenceMisc(),volumeMetricsReport,queryCount++,ActivitiesEnums.Checker,businessDate,nextBusinessDate);
     //   getDataFromDbAndAddItToMap(ActivitiesEnums.CheckerChequeBookGtsStp , queriesConfig.getQueryForCheckerChequeBookGtsStp(),volumeMetricsReport,queryCount++,ActivitiesEnums.Checker,businessDate,nextBusinessDate);
        System.out.println("====Checker Table Queries finished====");

    }

    public void getDataFromDbAndAddItToMap(ActivitiesEnums activityNameMakerChecker, String QUERY,  VolumeMetricsReport volumeMetricsReport,int queryCount, ActivitiesEnums makerOrChecker,Date start, Date end) {

        logger.info("query=" + QUERY);
        System.out.println("Executing Query number" + queryCount);
        jdbcTemplate.query(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                PreparedStatement ps = connection.prepareStatement(QUERY);

                java.sql.Date date = new java.sql.Date(start.getTime());
                 java.sql.Date upperLimit = new java.sql.Date(end.getTime());

                 ps.setString(1, formateDate(date));
                ps.setString(2, formateDate(upperLimit));
                return ps;
            }
        }, new RowCallbackHandler() {
            @Override
            public void processRow(ResultSet resultSet) throws SQLException {

              //  int count = resultSet.getInt("count");

                String makerOrCheckerName = resultSet.getString("done_by");

                int noOfEdms = resultSet.getInt("requestcount");

                int noOfTransactions = resultSet.getInt("transactioncount");

                if(makerOrChecker.equals(ActivitiesEnums.Maker))
                {
                    volumeMetricsReport.getMakerNameSet().add(makerOrCheckerName);
                }else
                {
                    volumeMetricsReport.getCheckerNameSet().add(makerOrCheckerName);

                }

                volumeMetricsReport.addDataToProductivityMakerCheckerMap(activityNameMakerChecker, makerOrCheckerName, noOfEdms,noOfTransactions);
            }
        });
        
    }
    ///////////TODo: //////////////// Dump Sheets Creation code --Query have to be executed only one time////////////////////////////////

    public void executeAllQueriesForDump(Date businessDate,Date nextBusinessDate, Date thisMonthStart, Date previousMonthStart, Date previousMonthEnd) throws Exception {
        //Maker ExcelWorkBook
        Workbook workbook1 = new XSSFWorkbook();
        createExcelFileDump(queriesConfig.getQueryForMakerMtd(),workbook1, "Mtd",thisMonthStart,businessDate);
        createExcelFileDump(queriesConfig.getQueryForMakerPreviousMonth(),workbook1, "PreviousMonth",previousMonthStart,previousMonthEnd);

        createExcelFileDump(queriesConfig.getQueryForMakerAccountOpeningFlex(),workbook1, "AccountOpeningFlex",businessDate,nextBusinessDate);
        createExcelFileDump(queriesConfig.getQueryForMakerKycFlex(),workbook1, "KycFlex",businessDate,nextBusinessDate);
        createExcelFileDump(queriesConfig.getQueryForMakerKycUnfreezeFlex(),workbook1, "KycUnfreezeFlex",businessDate,nextBusinessDate);
        createExcelFileDump(queriesConfig.getQueryForMakerAccountClosure(),workbook1, "AccountClosure",businessDate,nextBusinessDate);
        createExcelFileDump(queriesConfig.getQueryForMakerFixedDepositsForBreakageRenewals(),workbook1, "FixedDepositsForBreakageRenewals",businessDate,nextBusinessDate);
        createExcelFileDump(queriesConfig.getQueryForMakerSignatureMandateAdditionOrAmmendment(),workbook1, "SignatureMandateAdditionOrAmmendment",businessDate,nextBusinessDate);
        createExcelFileDump(queriesConfig.getQueryForCheckerTradeLicense(),workbook1, "TradeLicense",businessDate,nextBusinessDate);
        createExcelFileDump(queriesConfig.getQueryForCheckerAccountMaintanenceMisc(),workbook1, "AccountMaintanenceMisc",businessDate,nextBusinessDate);


        try {
                                                      //todo : in Remote directory change it................///
            FileOutputStream out = new FileOutputStream(new File(queriesConfig.getExcelDumpFileLocationOne() +"MakerDump.xlsx"));
            workbook1.write(out);
            out.close();
            logger.info(queriesConfig.getExcelDumpFileLocationOne() + "MakerDump written successfully on disk.");
        } catch (Throwable e) {
            logger.info("Catch Block: Error in writting file ");
            e.printStackTrace();
        }




        //Checker ExcelWorkBook
        Workbook workbook2 = new XSSFWorkbook();
        createExcelFileDump(queriesConfig.getQueryForCheckerMtd(),workbook2, "Mtd",thisMonthStart,businessDate);
        createExcelFileDump(queriesConfig.getQueryForCheckerPreviousMonth(),workbook2, "PreviousMonth",previousMonthStart,previousMonthEnd);

        createExcelFileDump(queriesConfig.getQueryForCheckerAccountOpeningFlex(),workbook2, "AccountOpeningFlex",businessDate,nextBusinessDate);
        createExcelFileDump(queriesConfig.getQueryForCheckerKycFlex(),workbook2, "KycFlex",businessDate,nextBusinessDate);
        createExcelFileDump(queriesConfig.getQueryForCheckerKycUnfreezeFlex(),workbook2, "KycUnfreezeFlex",businessDate,nextBusinessDate);
        createExcelFileDump(queriesConfig.getQueryForCheckerAccountClosure(),workbook2, "AccountClosure",businessDate,nextBusinessDate);
        createExcelFileDump(queriesConfig.getQueryForCheckerFixedDepositsForBreakageRenewals(),workbook2, "FixedDepositsForBreakageRenewals",businessDate,nextBusinessDate);
        createExcelFileDump(queriesConfig.getQueryForCheckerSignatureMandateAdditionOrAmmendment(),workbook2, "SignatureMandateAdditionOrAmmendment",businessDate,nextBusinessDate);
        createExcelFileDump(queriesConfig.getQueryForCheckerTradeLicense(),workbook2, "TradeLicense",businessDate,nextBusinessDate);
        createExcelFileDump(queriesConfig.getQueryForCheckerAccountMaintanenceMisc(),workbook2, "AccountMaintanenceMisc",businessDate,nextBusinessDate);


        try {
            //todo : in Remote directory change it................///
            FileOutputStream out = new FileOutputStream(new File(queriesConfig.getExcelDumpFileLocationTwo() +"CheckerDump.xlsx"));
            workbook2.write(out);
            out.close();
            logger.info(queriesConfig.getExcelDumpFileLocationTwo() + "CheckerDump written successfully on disk.");
        } catch (Throwable e) {
            logger.info("Catch Block: Error in writting file ");
            e.printStackTrace();
        }
    }



    public void createExcelFileDump(String excelQueriesForDump,Workbook workbook,String sheetNamesForDump,Date start,Date end) throws Exception {
        logger.info("createExcelFileExtract(-,-,-) started");
        //key-sheet name value-sheet data
        Map<String, List<Object[]>> reportMap = new LinkedHashMap<>();

        List<Object[]> list = executeQuery(excelQueriesForDump,start,end);
        reportMap.put(sheetNamesForDump, list);

        int rownum = 0;
        for (Map.Entry<String, List<Object[]>> keyValue : reportMap.entrySet()) {

            String sheetName = keyValue.getKey();
            List<Object[]> listSheet = keyValue.getValue();

            Sheet sheet = workbook.createSheet(sheetName);
            //todo : in dump  workbook.setSheetOrder(sheetName,4);
            rownum = 0;

            for (Object[] object : listSheet) {
                if (listSheet != null) {
                    Row currentDatarow = sheet.createRow(rownum++);
                    int cellnum = 0;
                    if (object != null) {
                        for (Object obj : object) {
                            Cell cell = currentDatarow.createCell(cellnum++);
                            //      cell.setCellValue((String) obj);
                            if (obj instanceof String) {
                                cell.setCellValue((String) obj);
                            } else if (obj instanceof Integer) {
                                cell.setCellValue((Integer) obj);
                            } else if (obj instanceof Double) {
                                double doubleValue = (Double) obj;
                                cell.setCellValue(doubleValue); //
                            } else if (obj instanceof Float) {
                                float floatValue = (Float) obj;
                                cell.setCellValue(floatValue);
                            } else {
                                cell.setCellValue(String.valueOf(obj));
                            }
                        }
                    }
                }
            }


        }

    }




    private List<Object[]> executeQuery(String query ,Date start,Date end ) throws Exception {


        final boolean[] header = {true};
        List<Object[]> results = new ArrayList<Object[]>();
        int count = 0;

        logger.info("query in of executeQuery(-,-,-) of DAO :" + query);
        jdbcTemplate.query(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                PreparedStatement ps = con.prepareStatement(query);
               java.sql.Date date = new java.sql.Date(start.getTime());
              java.sql.Date upperLimit = new java.sql.Date(end.getTime());
                ps.setString(1, dateFormat.format(date));
                ps.setString(2, dateFormat.format(upperLimit));

                // ps.setString(1, formateDate(date));
                //ps.setString(2, formateDate(upperLimit));
                //  logger.info("businessDate in executeQuery(-,-,-) :" + businessDate);
                // logger.info("nextBusinessDate in executeQuery(-,-,-) :" + nextBusinessDate);

                logger.info("Prepared Statment before bind in executeQuery(-,-,-):" + ps.toString());

                return ps;
            }
        }, new RowMapper<Object[]>() {
            @Override
            public Object[] mapRow(ResultSet rs, int rowNum) throws SQLException {

                //System.out.println("mapRow()-------");

                int columnCount = rs.getMetaData().getColumnCount();

                List<Object> columnNames = new ArrayList<>();

                if (header[0]) {
                    for (int i = 1; i <= columnCount; i++) {
                        columnNames.add(rs.getMetaData().getColumnName(i));
                        logger.info("Columnn Name: " + rs.getMetaData().getColumnName(i));
                        //System.out.println("column name :"+rs.getMetaData().getColumnName(i));
                    }
                    header[0] = false;
                    results.add(columnNames.toArray());
                    columnNames.clear();

                    populateData(rs, columnCount, columnNames);
                } else {
                    populateData(rs, columnCount, columnNames);
                }
                return columnNames.toArray();
            }

            private void populateData(ResultSet rs, int columnCount, List<Object
                    > columnNames) throws SQLException {


                for (int i = 1; i <= columnCount; i++) {
                    Object data = null;
                    if (rs.getObject(i) != null) {
                        data = rs.getObject(i);
                    }
                    columnNames.add(data);
                }

                results.add(columnNames.toArray());
            }
        });
        return results;
    }

}
